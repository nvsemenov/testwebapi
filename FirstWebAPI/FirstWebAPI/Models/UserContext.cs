﻿using System.Data.Entity;

namespace FirstWebAPI.Models
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}