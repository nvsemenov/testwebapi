﻿using System;

namespace FirstWebAPI.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte Age { get; set; }
    }
}