﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;
using FirstWebAPI.Models;

namespace FirstWebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        #region Fields

        private readonly UserContext _userContext;

        #endregion

        #region Constructors

        public ValuesController()
        {
            _userContext = new UserContext();
        }

        #endregion

        #region Methods

        // GET api/values
        public IEnumerable<User> Get()
        {
            return _userContext.Users;
        }

        // GET api/values/id
        public User Get(Guid id)
        {
            return _userContext.Users.Find(id);
        }

        // POST api/values
        public void Post([FromBody] User value)
        {
            _userContext.Users.Add(value);
            _userContext.SaveChanges();
        }

        // PUT api/values/id
        public void Put(Guid id, [FromBody] User value)
        {
            if (id == value.Id)
            {
                _userContext.Entry(value).State = EntityState.Modified;
                _userContext.SaveChanges();
            }
        }

        // DELETE api/values/id
        public void Delete(int id)
        {
            var user = _userContext.Users.Find(id);
            if (user != null)
            {
                _userContext.Users.Remove(user);
                _userContext.SaveChanges();
            }
        }

        #endregion
    }
}